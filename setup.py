import os
import platform
import subprocess
import sys
import tempfile
from pathlib import Path

from setuptools import find_packages, setup

PROJECT_NAME = "Sorter"
DESCRIPTION = "Sort files by extentions or by modification date."
AUTHOR = "Amelien Deshams"
AUTHOR_EMAIL = "a.deshams+git@slmail.me"
CURRENT_DIR = Path(__file__).parent
README_FILEPATH = CURRENT_DIR / "README.md"
REQUIREMENTS_FILEPATH = CURRENT_DIR / "requirements.txt"
if README_FILEPATH.is_file():
    LONG_DESCRIPTION = README_FILEPATH.read_text(encoding="utf8")
else:
    LONG_DESCRIPTION = "Unable to load README.md"
REQUIREMENTS = REQUIREMENTS_FILEPATH.read_text().splitlines() if REQUIREMENTS_FILEPATH.is_file() else []
VERSION = (CURRENT_DIR / "sorter" / "_version.py").read_text().split('"')[1]
EXTRAS_REQUIRE = {
    "documentation": [
        "recommonmark",
        "sphinx",
        "sphinx-automodapi",
        "sphinx-copybutton",
        "sphinx_rtd_theme",
        "sphinxcontrib-napoleon",
    ],
    "tests": [
        "pytest",
    ],
}


def _create_shortcut_to_shell_sendto(filepath: Path) -> None:
    shortcut_file_name = filepath.with_suffix(".lnk").name
    if not (appdata := os.getenv("APPDATA")):
        sys.stderr("Cannot get the environment variable 'APPDATA', skip creating shortcut to shell:sendto.")
        return
    shortcut_file = Path(appdata) / "Microsoft" / "Windows" / "SendTo" / shortcut_file_name
    script = f"""
    Set oWS = WScript.CreateObject("WScript.Shell")
    sLinkFile = "{shortcut_file}"
    Set oLink = oWS.CreateShortcut(sLinkFile)
    oLink.TargetPath = "{filepath}"
    oLink.Save
    """
    with tempfile.TemporaryDirectory() as tmpdir:
        bat_file = Path(tmpdir) / "CreateShortcut.vbs"
        with bat_file.open("w", encoding="utf8") as f:
            f.write(script)
        subprocess.call([r"C:\Windows\system32\cscript.exe", bat_file], cwd=tmpdir)


setup(
    name=PROJECT_NAME,
    version=VERSION,
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    project_urls={
        "Source": "https://gitlab.com/ameliend/sorter",
        "Documentation": "https://ameliend.gitlab.io/sorter",
    },
    classifiers=[  # https://pypi.org/classifiers/
        "Development Status :: 5 - Production/Stable",
        "Environment :: Console",
        "Environment :: Win32 (MS Windows)",
        "License :: OSI Approved :: MIT License",
        "Intended Audience :: Developers",
        "Operating System :: Microsoft :: Windows",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
        "Topic :: Utilities",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Typing :: Typed",
    ],
    packages=find_packages(exclude=["tests"]),
    license="MIT",
    python_requires=">=3.8",
    install_requires=REQUIREMENTS,
    extras_require=EXTRAS_REQUIRE,
    include_package_data=True,
    entry_points={
        "console_scripts": [
            "sort_by_date = sorter.cli:sort_by_date_cli",
            "sort_by_ext = sorter.cli:sort_by_ext_cli",
        ]
    },
)
if platform.system() == "Windows":
    _create_shortcut_to_shell_sendto(CURRENT_DIR / "sorter" / "sort_by_date.py")
    _create_shortcut_to_shell_sendto(CURRENT_DIR / "sorter" / "sort_by_ext.py")
