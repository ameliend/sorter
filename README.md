[![coverage](https://gitlab.com/ameliend/sorter/badges/main/coverage.svg)](https://gitlab.com/ameliend/sorter/-/commits/main)
[![vscode-editor](https://badgen.net/badge/icon/visualstudio?icon=visualstudio&label)](https://code.visualstudio.com/)
[![uv](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/uv/main/assets/badge/v0.json)](https://github.com/astral-sh/uv)
[![Ruff](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/ruff/main/assets/badge/v2.json)](https://github.com/astral-sh/ruff)
[![Copier](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/copier-org/copier/master/img/badge/badge-grayscale-inverted-border-orange.json)](https://github.com/copier-org/copier)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
[![API Documentation](https://badgen.net/badge/icon/API%20documentation?icon=gitlab&label&color=cyan)](https://ameliend.gitlab.io/sorter)

# Sorter

<p align="center">
  <img src="./resources/logo.png">
</p>

These 3 scripts help me to sort a list of files.
They help me to sort a list of files by their extensions, or by their date, by moving them in adapted folders.

## 📥 Installation (sort_by_date / sort_by_ext)

1. Download the source code by clicking **Code** > **zip**, and unzip it somewhere on your computer.

<p align="center">
  <img src="./resources/download.png">
</p>

2. Navigate to where you unzipped source code using the command `cd`, for example:

    ```
    cd C:\Users\your_user\Downloads\sorter
    ```

3. Install the Sorter (Make sure to install Python 3.8+ first)

    ```
    pip install .
    ```

You should see the two shortcuts **"sort_by_date"** and **"sort_by_ext"** appear in the **Send to** menu.

<p align="center">
  <img src="./resources/shell_send_to.png">
</p>

I just have to select one or several files and then click on "Send to".

Now you can select one or more files and do **Send to**.

## 📥 Installation (unity_sorter)

It helps me organize a **Untity** project,
it will scan all the files in sub-folders and put them in the right folders, keeping the .meta files.

* It does not require any dependencies and should therefore work without issues on any computer with **Python 3.8+**.
* It will generate a **"sorter_report.log"** file to see what changed.


> ⚠️ I use the unity_sorter.py for my own purpose, I recommend you to make a backup of your project
  before using it. It can move files you don't want to, and can break your project.

1. Download the source code of the Sorter by clicking **Code** > **zip**, and unzip it somewhere on your computer.

<p align="center">
  <img src="./resources/download.png">
</p>

2. Copy **unity_sorter.py** (inside the "sorter" folder) to your Unity project root and you can just run it with your default python executable (by double clinking on it).

