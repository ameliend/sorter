"""Sort a unity project files by creating their extension folder, and manage their meta."""

import logging
import os
import shutil
from pathlib import Path

CURRENT_DIR = Path(__file__).parent
EXCLUDED_FOLDERS = [  # Add any folder here you want to exclude for sorting
    "VRChat Examples",
    "Udon",
    "CyanEmu",
    "CyanTrigger",
    "EasyQuestSwitch",
    "AmplifyShaderEditor",
    "Bakery",
    "VRCPrefabs",
    "VRWorldToolkit",
    "_PoiyomiShaders",
    "Cibbi's shaders",
    "TextMesh Pro",
    "Lyuma",
    "GestureManager",
    "HoshinoLabs",
    "LightShaftProjector",
    "VRLabs",
    "Doppelgänger",
    "Mochie",
]
FILE_CATEGORIES = {  # Enter the name of a folder where you want to store the list of given extensions
    "Animations": [".anim"],
    "Blender Files": [".blend"],
    "Scenes": [".unity"],
    "Materials": [".mat"],
    "Menu": [".asset", ".controller", ".mask"],
    "Brushes": [".brush"],
    "Shaders": [".shader"],
    "Presets": [".preset"],
    "Prefabs": [".prefab"],
    "Fonts": [".ttf", ".otf", ".woff", ".woff2", ".eot", ".svg"],
    "Meshes": [
        ".obj",
        ".fbx",
        ".stl",
        ".dae",
        ".3ds",
        ".ply",
        ".x3d",
        ".amf",
        ".step",
        ".stp",
        ".iges",
        ".igs",
        ".skp",
        ".assimp",
    ],
    "Textures": [
        ".jpg",
        ".jpeg",
        ".png",
        ".gif",
        ".bmp",
        ".tiff",
        ".tif",
        ".webp",
        ".exr",
        ".svg",
        ".psd",
        ".pdf",
        ".raw",
        ".hdr",
    ],
    "Sounds": [
        ".mp3",
        ".wav",
        ".ogg",
        ".flac",
        ".aac",
        ".wma",
        ".m4a",
        ".aiff",
        ".au",
        ".mid",
        ".midi",
        ".amr",
        ".opus",
    ],
}

# Logging stuff
logger = logging.getLogger("Sorter")
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s :: %(name)s :: %(levelname)s :: %(message)s")
handler = logging.FileHandler(CURRENT_DIR / "sorter_report.log")
handler.setLevel(logging.DEBUG)
handler.setFormatter(formatter)
logger.addHandler(handler)


def _move_file(source: Path, destination: Path) -> None:
    if destination.exists():
        return
    destination.parent.mkdir(exist_ok=True)
    try:
        shutil.move(source, destination)
        logger.info("File '%s' moved to: '%s'", source, destination)
    except PermissionError:
        logger.exception()


def sort_project() -> None:
    """Sort a Unity project's files by creating their extension folder and managing meta files."""
    categories_by_extentions = {ext: category for category, exts in FILE_CATEGORIES.items() for ext in exts}
    extensions_to_sort = [ext for sublist in FILE_CATEGORIES.values() for ext in sublist]
    for item in CURRENT_DIR.glob("**/*"):
        item_extension = item.suffix.lower()
        if item_extension not in extensions_to_sort or any(
            excluded_item in str(item) for excluded_item in EXCLUDED_FOLDERS
        ):  # Exclude extensions not included in FILE_CATEGORIES, and exclude files in EXCLUDED_FOLDERS
            continue
        item_meta = f"{item}.meta"
        destination = CURRENT_DIR / categories_by_extentions[item_extension] / item.name
        destination_meta = destination.with_name(f"{item.name}.meta")
        _move_file(item, destination)
        _move_file(item_meta, destination_meta)  # Also move its meta file
        if item_extension in FILE_CATEGORIES["Textures"]:  # Fix meta texture file
            fix_meta_texture(destination_meta)


def fix_meta_texture(meta_file: Path) -> None:
    """Fix some metadata values in a given meta file.

    Parameters
    ----------
    meta_file : Path
        The path of the meta file to be fixed.
    """
    replacements = {
        "streamingMipmaps: 0": "streamingMipmaps: 1",
        "alphaIsTransparency: 0": "alphaIsTransparency: 1",
        "crunchedCompression: 0": "crunchedCompression: 1",
        "compressionQuality: 50": "compressionQuality: 100",
    }
    content = meta_file.read_text(encoding="utf8")
    modified = False
    for search, replace in replacements.items():
        if search in content:
            content = content.replace(search, replace)
            logger.debug("Fixed %s for file: '%s'", search.split(":", maxsplit=1)[0], meta_file)
            modified = True
    if modified:
        meta_file.write_text(content)


def delete_empty_folders() -> None:
    """Recursively scan through directories and delete any empty folders found."""
    scan_pass = 0
    while True:
        item_to_delete = [item for item in CURRENT_DIR.glob("**/**") if os.listdir(item) == []]
        scan_pass += 1
        for item in item_to_delete:
            item.rmdir()
            item_meta = item.with_suffix(".meta")
            item_meta.unlink()
            logger.info("Removed empty item '%s'", item)
        if not item_to_delete:
            return


if __name__ == "__main__":
    sort_project()
    delete_empty_folders()
