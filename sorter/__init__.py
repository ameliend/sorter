"""Top-level package for Sorter."""

from sorter._version import __version__
from sorter.sort_by_date import sort_files_by_date
from sorter.sort_by_ext import sort_files_by_extensions

__author__ = """Amelien Deshams"""
__email__ = "a.deshams+git@slmail.me"

__all__ = [
    "__version__",
    "sort_files_by_date",
    "sort_files_by_extensions",
]
