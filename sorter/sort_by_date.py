"""Sort files by their date taken."""

from __future__ import annotations

import mimetypes
import shutil
from datetime import datetime
from pathlib import Path
from sys import argv

import ffmpeg
from logger import Logger
from PIL import Image

logger = Logger("Sorter")


def _move_file(source: Path, destination: Path, overwrite: bool = False) -> None:
    if not overwrite and destination.exists():
        logger.error("The destination file '%s' already exists.", destination, notify=True)
        return
    destination.parent.mkdir(exist_ok=True)
    try:
        shutil.move(source, destination)
    except PermissionError:
        logger.exception(notify=True)
        return
    logger.success(f"File '{source}' moved to : '{destination}'")


def _is_image(filepath: str) -> str | None:
    mime_type, _ = mimetypes.guess_type(filepath)
    return mime_type and mime_type.startswith("image")


def _is_video(filepath: str) -> str | None:
    mime_type, _ = mimetypes.guess_type(filepath)
    return mime_type and mime_type.startswith("video")


def sort_files_by_date(filepaths: list[str], overwrite: bool = False) -> None:
    """Sort files by their date taken.

    Using metadata if available. If no metadata is found, it falls back to sorting by modification date.
    The sorted files are then moved to new directories named after their year and month.

    Parameters
    ----------
    filepaths : List[str]
        A list of strings representing the paths to the files that will be sorted.
    overwrite : bool, optional
        Whether existing files in the destination directory should be overwritten. Defaults to False.
    """
    for item in filepaths:
        path_item = Path(item)
        if path_item.is_dir():
            continue
        date_taken = None
        if _is_image(path_item):
            if date_taken := get_image_date_taken(path_item):
                datetime_obj = datetime.strptime(date_taken, "%Y:%m:%d %H:%M:%S")
        elif _is_video(path_item) and (date_taken := get_video_media_created(path_item)):
            datetime_obj = datetime.strptime(date_taken, "%Y-%m-%dT%H:%M:%S.%fZ")
        if not date_taken:
            logger.warning("No metadata found for '%s', sorting by modification date.", path_item)
            datetime_obj = datetime.fromtimestamp(path_item.stat().st_mtime)
        destination = path_item.parent / datetime_obj.strftime("%Y-%m") / path_item.name
        _move_file(path_item, destination, overwrite)


def get_image_date_taken(filepath: str) -> str | None:
    """Get the date taken from an image's EXIF metadata.

    Parameters
    ----------
    filepath : str
        A string representing the location of an image file.

    Returns
    -------
    Optional[str]
        The date taken from the image's metadata in "YYYY:MM:DD HH:MM:SS" format if found; otherwise, None.
    """
    image = Image.open(filepath)
    try:
        exif_data = image._getexif()  # noqa: SLF001
        if not exif_data.get(36867):
            exif_data = image.getexif()
    except AttributeError:
        exif_data = image.getexif()
    return exif_data.get(36867)


def get_video_media_created(filepath: str) -> str | None:
    """Get the creation time of a video file from its metadata.

    Parameters
    ----------
    filepath : str
        A string representing the location of a video file.

    Returns
    -------
    Optional[str]
        The creation time of the video file in "YYYY-MM-DDTHH:MM:SS" format if found; otherwise, None.
    """
    probe = ffmpeg.probe(filepath)
    metadata = probe["format"]["tags"]
    return metadata.get("creation_time")


if __name__ == "__main__":
    logger.debug(argv[1:])
    sort_files_by_date(filepaths=argv[1:], overwrite=True)
