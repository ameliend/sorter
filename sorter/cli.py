"""Console script for Sorter."""

from sys import argv

from logger import Logger

from sorter import sort_files_by_date, sort_files_by_extensions

logger = Logger("Sorter")


def sort_by_date_cli() -> None:
    """Console script for sort_by_date."""
    logger.debug(argv[1:])
    sort_files_by_date(filepaths=argv[1:], overwrite=True)


def sort_by_ext_cli() -> None:
    """Console script for sort_by_ext."""
    logger.debug(argv[1:])
    sort_files_by_extensions(filepaths=argv[1:], overwrite=True)
