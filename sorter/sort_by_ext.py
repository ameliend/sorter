"""Sort files by their extensions."""

from __future__ import annotations

import shutil
from pathlib import Path
from sys import argv

from logger import Logger

logger = Logger("Sorter")


def _move_file(source: Path, destination: Path, overwrite: bool = False) -> None:
    if not overwrite and destination.exists():
        logger.error("The destination file '%s' already exists.", destination, notify=True)
        return
    destination.parent.mkdir(exist_ok=True)
    try:
        shutil.move(source, destination)
    except PermissionError:
        logger.exception(notify=True)
        return
    logger.success(f"File '{source}' moved to : '{destination}'")


def sort_files_by_extensions(filepaths: list[str], overwrite: bool = False) -> None:
    """Sort files by their extensions.

    Parameters
    ----------
    filepaths : List[str]
        A list of paths to the files that need sorting.
    overwrite : bool, optional
        If True it allows overwriting existing files at destination path, by default False.
    """
    for item in filepaths:
        path_item = Path(item)
        if path_item.is_dir():
            destination = path_item.parent / "Folders" / path_item.name
        else:
            if (ext := path_item.suffix[1:]) == "":
                ext = "Unknown Extension"
            destination = path_item.parent / ext.capitalize() / path_item.name
        _move_file(path_item, destination, overwrite)


if __name__ == "__main__":
    logger.debug(argv[1:])
    sort_files_by_extensions(filepaths=argv[1:], overwrite=True)
