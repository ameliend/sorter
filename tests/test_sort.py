from datetime import datetime
from os.path import getmtime

from sorter import sort_by_date, sort_by_ext


def test_sort_by_date(sample_file):
    expected_date = datetime.fromtimestamp(getmtime(sample_file)).strftime("%Y-%m")
    expected_filepath = sample_file.parent.joinpath(expected_date, sample_file.name)
    sort_by_date.sort_files_by_date([str(sample_file)])
    assert expected_filepath.exists()


def test_sort_by_ext(sample_file):
    expected_filepath = sample_file.parent.joinpath("Txt", sample_file.name)
    sort_by_ext.sort_files_by_extensions([str(sample_file)])
    assert expected_filepath.exists()
