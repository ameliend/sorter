## [1.0.2](https://gitlab.com/ameliend/sorter/compare/v1.0.1...v1.0.2) (2024-11-01)

### 🦊 CI/CD

* **setup:** fix encoding issue ([4f16b1a](https://gitlab.com/ameliend/sorter/commit/4f16b1a86eab426f92803e5687562d495c408ea1))

## [1.0.1](https://gitlab.com/ameliend/sorter/compare/v1.0.0...v1.0.1) (2024-11-01)

### 🦊 CI/CD

* **ruff:** top-level linter settings are deprecated in favour of their counterparts in the `lint` section ([1460be5](https://gitlab.com/ameliend/sorter/commit/1460be54521d20183945565df9f8ef1f592ed02c))
* **setup:** add Windows OS check to create SendTo shortcuts ([946b769](https://gitlab.com/ameliend/sorter/commit/946b7691d0ff55aaeea1a1819fc97b3fc73b55be))

## [1.0.0](https://gitlab.com/ameliend/sorter/compare/...v1.0.0) (2024-11-01)

### 📔 Docs

* **README:** update README ([d6ebb49](https://gitlab.com/ameliend/sorter/commit/d6ebb493fe0470701d8e96e57df4d28e463bf55d))
* **sphinx:** update logo ([543ce13](https://gitlab.com/ameliend/sorter/commit/543ce13163cc246e8c5a7a353cef3613412c66d8))

### 🦊 CI/CD

* **setup:** revert pyproject.toml workflow to setup.py ([9dc3f68](https://gitlab.com/ameliend/sorter/commit/9dc3f684c73014c45b039c66bc4402cde6e69be0))

### 🧪 Tests

* add tests ([d084959](https://gitlab.com/ameliend/sorter/commit/d084959e591b6bc094a74523afa8480874f084a4))

### 🚀 Features

* initial commit ([88ad676](https://gitlab.com/ameliend/sorter/commit/88ad676b9e80ec8be2b1ab580c7c3b3f3b9bfd09))
* initial commit ([5de1bf9](https://gitlab.com/ameliend/sorter/commit/5de1bf955d7b9b23a0f16e917741aec445838d2a))
